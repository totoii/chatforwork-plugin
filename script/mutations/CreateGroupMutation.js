
import Relay from 'react-relay';

export default class CreateGroupMutation extends Relay.Mutation {
  
  getMutation() {
    return Relay.QL`mutation{createGroup}`;
  }
  getFatQuery() {
    return Relay.QL`
      fragment on CreateGroupPayload @relay(pattern: true) {
        viewer{
          allgroups{
            id,
            name
          }
        }
      }
    `;
  }
  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'viewer',
      parentID: this.props.viewer.id,
      connectionName: '',
      edgeName: 'allgroups',
      rangeBehaviors: () => {
          return 'append';
      },
    }];
  }
  getVariables() {
    return {
      me:JSON.parse(localStorage.userinfo).userid,
      groupname: this.props.groupname,
    };
  }
}
