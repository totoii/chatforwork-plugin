import React, { Component } from 'react';
import fetch from 'isomorphic-fetch'
import {host_ip,port_app,appid} from '../components/constant'

export default class Plugin extends Component {
	start(){
		fetch(host_ip+port_app+"/plug/client",{
			method: 'post',
			headers: {
		        'Content-Type':"application/json"
		    },
			body:JSON.stringify({
				"email":appid,
				"name":appid+" user",
			})
		})
		.then(response => response.json())
		.then(json => {  	
		  	localStorage.setItem("clientinfo",JSON.stringify(json));
		  	localStorage.setItem("subject",0);
		  	localStorage.setItem("chatid",json.roomid);
		  	this.props.success(json.guestid,0,json.roomid);
		})
	}

	componentDidMount() {
		this.start();
	}

	render() {
		return (
			<div className="chat-form">
				<div style={{textAlign:"center"}}>
					connecting to chatforwork
				</div>
			</div>
		);
	}
}
