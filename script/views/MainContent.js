import React, { Component } from 'react';
import Register from './Register'
import Chat from './Chat'
import HomePage from './HomePage'
import {frontdesk} from '../components/constant'


export default class MainContent extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	chatid: this.getChatID(),
	    	guestid: this.getGuestID(),
	    	subject:this.getSubjectID()
	    };
  	}

  	getGuestID(){
  		if(JSON.parse(localStorage.getItem("clientinfo"))){
	      return JSON.parse(localStorage.getItem("clientinfo")).guestid;
	    }
	    return "";
  	}
  	getSubjectID(){
  		if(localStorage.getItem("subject")){
	      return localStorage.getItem("subject");
	    }
	    return "";
  	}
  	getChatID(){
  		if(localStorage.getItem("chatid")){
	      return localStorage.getItem("chatid");
	    }
	    return "";
  	}

	render() {
		{
			console.log(this.props.params)
		}
		return (
			<div>
			{
				frontdesk?
				(
					<div className="main-content">
					{
						this.state.guestid?
						<Chat subject={this.state.subject} guestid={this.state.guestid} chatid={this.state.chatid}/>:
						<Register success={this.registerSuccess.bind(this)}/>
					}
					</div>
				):
				(
					<div className="main-content-backend">
					{
						this.state.guestid?
						<HomePage subject={this.state.subject} guestid={this.state.guestid} chatid={this.state.chatid}
						params={this.props.params}/>:
						<Register success={this.registerSuccess.bind(this)}/>
					}
					</div>
				)
			}
			</div>
		);
	}
	
	registerSuccess(id,subjectid,chatid){
		this.setState({
			guestid:id,
			subject:subjectid,
			chatid:chatid,
		})
	}
}
