import React, { Component } from 'react';
import Relay from 'react-relay'
import ChatList from '../components/chat/ChatList';
import ChatInput from '../components/chat/ChatInput';
import VDOCall from '../components/chat/VDOCall'
import FriendList from '../components/chat/FriendList'
import AddSubject from '../components/chat/AddSubject'
import fetch from 'isomorphic-fetch'
import PeerConnection from '../components/VdoCall/PeerConnection'

const socket = require('socket.io-client')();

class ChatPanel extends Component {

  constructor(props) {
    super(props);
    this.state={
      localSrc: "",
      peerSrc: "",
      caller:"",
      callActive:"",
      isCalling:"",
      showpanel: "chat",
      isNewSubject: false,
      isNewMember:[]
    }

    this.pc = {};
    this.config = null;
  }

  getMe(){
    if(JSON.parse(localStorage.getItem("userinfo"))){
      return JSON.parse(localStorage.getItem("userinfo")).userid;
    }
    return "";
  }

  getFriendID(){
    if(this.props.params.roomtype=="direct"){
      return this.props.params.roomid;
    }
    return "";
  }

  updateData(){
    this.props.relay.forceFetch({
      roomtype: this.props.params.roomtype,
      my_id:this.getMe(),
      friend_id:this.props.params.roomid,
    });
  }

  subscribeData(){
    
    
    // console.log(this.props.data.chatroom.id)
    socket.emit('join_room',this.props.data.chatroom.id,this.getMe());
    socket.emit('msg_read',this.props.data.chatroom.id,this.getMe());
    socket.on('data_received', this.ioDataReceived.bind(this));

    if(this.props.params.roomtype=="support"){
      socket.on('plugin_receive',(data)=>{alert(data.data)});
      return;
    }

    // socket.on('msg_received', this.ioDataReceived.bind(this));
    socket.on('invited_member', this.onInvitedMember.bind(this));
    socket.on('data_received', this.ioDataReceived.bind(this));
    socket.on('request',(data) => {this.setState({ isCalling: true,caller: data["from"]})});
    socket.on("call", data => {
        console.log("accept",data);
        if (data.sdp) {
          console.log("remote")
          this.pc.setRemoteDescription(data.sdp);
          if (data.sdp.type === "offer") this.pc.createAnswer();
        }
        else this.pc.addIceCandidate(data.candidate);
      })
    socket.on('chat_notification', this.onNotification.bind(this))  
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.location.pathname!=prevProps.location.pathname){
      this.updateData();
    }
   
    if(this.props.data.chatroom != null && prevProps.data.chatroom != null){
      if(this.props.data.chatroom.id!=prevProps.data.chatroom.id){
        this.subscribeData();
      }
    }
    else if(this.props.data.chatroom != null){
      this.subscribeData();
    }
  }

  componentDidMount() {
    this.updateData();
  }

  ioDataReceived(msg){
    this.props.relay.forceFetch();
    socket.emit('msg_read',this.props.data.chatroom.id,this.getMe());
  }

  onInvitedMember(roomid){
    window.location.href = '/groupnoname/' + roomid;
  }

  onNotification(msg){
    console.log(msg)
  }
  

  getRoomName(){
    if(this.props.params.roomtype=="direct"){
      if(this.props.data.chatroom.member.length==0)return "";
      if(this.props.data.chatroom.member[0].id!=this.getMe()){
        return this.props.data.chatroom.member[0].name;
      }
      return this.props.data.chatroom.member[1].name;
    }else if(this.props.params.roomtype=="group"){
      var group_dic = new Object();
      this.props.data.allgroups.map((data)=>{  
        group_dic[data.id]=data.name;
      });

      return group_dic[this.props.params.roomid];
    }else if(this.props.params.roomtype=="groupnoname"){
      if(this.props.data.chatroom.member.length==0)return "";
      var roomname = ""; 
      for (var i = 0; i < this.props.data.chatroom.member.length; i++) { 
        if(this.props.data.chatroom.member[i].id!=this.getMe()){
          roomname += this.props.data.chatroom.member[i].name;
          if(this.props.data.chatroom.member.length-1 != i){
            roomname += ","; 
          }
        }
      } 
      return roomname + "("+ this.props.data.chatroom.member.length +")";//group_dic[this.props.params.roomid];
    }else if(this.props.params.roomtype=="support"){
      return this.props.data.chatroom.guest.name+" ["+this.props.data.chatroom.guest.email+"]";
    }
  }

  getComponent() {
    switch (this.state.showpanel) {
		  case "friends":
			  return <FriendList 
            roomid={this.props.data.chatroom.id} 
            userlist={this.props.data.usernotinroom} 
            isNewMember={this.state.isNewMember} 
            socket={socket}
            showFriendList={this.state.showFriendList}
            cancelFriendToChatroom={this.cancelFriendToChatroom.bind(this)}
            addNewMemberToChatroom={this.addNewMemberToChatroom.bind(this)} />
			case "subject":
				return <AddSubject 
                isNewSubject={this.state.isNewSubject}
                allusers={this.props.data.allusers}
                cancelSubjectToChatroom={this.cancelSubjectToChatroom.bind(this)}
                saveSubjectToChatroom={this.saveSubjectToChatroom.bind(this)}
                roomid={this.props.data.chatroom.id}
                socket={socket}  />
      }
  }

	render() {

    if(this.props.data.chatroom==null)return null;

    var user_dic = new Object();
    this.props.data.chatroom.member.map((data)=>{  
      user_dic[data.id]=data.name;
    });
    if(this.props.data.chatroom.member.length==0){
      this.props.data.allusers.map((data)=>{  
        user_dic[data.id]=data.name;
      });
    }

    return (
      <div className="chat-panel">
        <div className="room-name">
          <div>
          {this.getRoomName()}
          </div>
          <div className="action">
            {
              // <i className="material-icons" onClick={this.callWithVideo(false);}>call</i>
            }
            <i className="material-icons" onClick={this.callWithVideo.bind(this,true,this.getFriendID())}>videocam</i>
            <i className="material-icons" onClick={this.addFriendToChatroom.bind(this)}>&#xE7FE;</i>
            <i className="material-icons" onClick={this.addSubject.bind(this)}>playlist_add</i>
          </div>
        </div>
        <VDOCall 
          callWithVideo={this.callWithVideo.bind(this)}
          localSrc={this.state.localSrc}
          caller={this.state.caller}
          peerSrc={this.state.peerSrc}
          isCalling={this.state.isCalling}
          callActive={this.state.callActive}/>
          {this.state.showpanel == "chat" ? (
          <div className="chat-panel">    
            <ChatList user_dic={user_dic} roomtype={this.props.params.roomtype} me={this.getMe()} msglist={this.props.data.chatroom.messages.edges}/>
            <ChatInput me={this.getMe()} roomid={this.props.data.chatroom.id} socket={socket}/>
          </div>  
          ) :this.getComponent() 
        }
          
      </div>
    );	
  }

  callWithVideo(isCaller,friendID) {
    var config = { video:true,audio: true };
    if(isCaller){
      this.setState({callActive:true});
    }
    this.config = config;
    this.pc = new PeerConnection(friendID)
      .on("localStream", src => 
        this.setState({ localSrc: src }))
      .on("peerStream", src => 
        this.setState({ peerSrc: src ,isCalling:false,callActive:true}))
      .start(isCaller, config,this.getMe())
    // return () => this.props.startCall(true, friendID, config);
  }

  addFriendToChatroom(){
    this.setState({showpanel: "friends"});
  }
  cancelFriendToChatroom(){
    this.setState({showpanel: "chat"});
  }
  addNewMemberToChatroom(){
    this.setState({showpanel: "chat"});
  }
  addSubject() {
    this.setState({showpanel: "subject", "isNewSubject": true});
  }
  cancelSubjectToChatroom(){
    this.setState({showpanel: "chat"});
  }
  saveSubjectToChatroom(){
    this.setState({showpanel: "chat"});
  }
}

export default Relay.createContainer(ChatPanel, {
    initialVariables: {
      roomtype: null,
      my_id:null,
      friend_id:null
    },
  	fragments: {
    	data: () => Relay.QL`
      		fragment on Viewer {
            chatroom(my_id:$my_id,friend_id:$friend_id,roomtype:$roomtype){
              id
              member{
                id
                name
              }
              guest{
                name
                email
              }
              messages(first:100){
                edges{
                  node{
                    id
                    sender
                    timestamp
                    message
                    filename
                    path
                    mimetype
                    readby{
                      id,
                      name
                    }
                  }
                }
              }
            }
            
            allusers{
                id
                name
            }
            allgroups{
                id
                name
            }
            usernotinroom(my_id:$my_id,friend_id:$friend_id){
                id,
                name
            }   
      		}
    	`,
  	},
});
