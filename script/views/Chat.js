import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import {host_ip,port_app} from '../components/constant'
import moment from 'moment'

var socket = require('socket.io-client')(host_ip+port_app);

class Chat extends Component {


	componentDidUpdate(){
		document.getElementsByClassName("message-container")[0].scrollTop=5000;
	}

	subscribeData(){
	    // console.log(this.props.data.chatroom.id)
	    socket.emit('plug_in',this.props.chatid,this.props.subject);
	    socket.on('data_received', this.ioDataReceived.bind(this));
	    // socket.emit('msg_read',this.props.data.chatroom.id,this.getMe());
	    // // socket.on('msg_received', this.ioDataReceived.bind(this));
	    // socket.on('data_received', this.ioDataReceived.bind(this));
	    // socket.on('request',(data) => {this.setState({ isCalling: true,caller: data["from"]})});
	    // socket.on("call", data => {
	    //     console.log("accept",data);
	    //     if (data.sdp) {
	    //       console.log("remote")
	    //       this.pc.setRemoteDescription(data.sdp);
	    //       if (data.sdp.type === "offer") this.pc.createAnswer();
	    //     }
	    //     else this.pc.addIceCandidate(data.candidate);
	    //   })
	}

	ioDataReceived(msg){
	    this.props.data.refetch();
	}

	componentDidMount() {
		this.subscribeData();
	}

	render() {
		let chat_list = null;
		let my_id = this.props.guestid;
		let prev_id ="";

		if(!(!this.props.data.viewer || !this.props.data.viewer.chatroom)){
			chat_list = this.props.data.viewer.chatroom.messages.edges.map(({node})=>{
							let prev_id_bak = prev_id;
							prev_id = node.sender;
							return (<Message key={node.id} data={node} prev_sender={prev_id_bak} is_client={my_id==node.sender}/>);
						});
		}

		return (
			<div className="chatforwork-plugin-panel">
				<div className="message-container">
					{
						chat_list
					}
				</div>
				<Input chatid={this.props.chatid} myid={this.props.guestid}/>
			</div>
		);
	}
}


class Message extends Component {
	render() {

		let style=this.props.is_client?"chat-message is-client":"chat-message";

		if(this.props.is_client){
			return (<Message_Right data={this.props.data}/>)
		}else{
			return (<Message_Left data={this.props.data}/>)
		}

		return (
			<p className={style}>
				{
					this.props.prev_sender==this.props.data.sender?
					null:
					<span className="user-name">
						{this.props.is_client?"Me":"PA Support"}
					</span>
				}
				<span className="message-p">
					{
						this.props.is_client?
							this.props.data.readby.length>1?
								<span className="read">Read</span>:null
						:null
					}
					<span className="message">
						{this.props.data.message}
					</span>
				</span>
			</p>
		);
	}
}

class Message_Left extends Component {
	render() {
		return (
			<div className="balloon-wraper">
				<div className="inner-container">
					<div className="balloon-left">
						{this.props.data.message}
					</div>
					<Item_Info timestamp={this.props.data.timestamp}/>
				</div>
			</div>
		);
	}
}

class Message_Right extends Component {
	render() {
		return (
			<div className="balloon-right-wrapper">
				<div className="inner-container">
					<Item_Info readby={this.props.data.readby} timestamp={this.props.data.timestamp}/>
					<div className="balloon-right">
						{this.props.data.message}
					</div>
				</div>
			</div>
		);
	}
}

class Item_Info extends Component {
	render() {
		let READ_TAG = null;
		if(this.props.readby&&this.props.readby.length>1){
			READ_TAG = (<div>Read</div>)
		}

		return (
			<div className="info">
				{READ_TAG}	
				{
					<div>{moment(new Date(this.props.timestamp)).format('HH:mm')}</div>
				}			
			</div>
		);
	}
}


class Input extends Component {

	onPress(e){
		if(!e.shiftKey&&e.key=="Enter"){
			// socket.emit('plugin_send',e.target.value,this.props.chatid,);
			socket.emit('msg_sended',e.target.value,this.props.chatid,this.props.myid);

			e.target.value="";
			e.preventDefault();
		}
	}

	render() {
		return (
			<div className="chat-input">
				<textarea placeholder="Type Message" onKeyPress={this.onPress.bind(this)}/>
			</div>
		);
	}
}

const CurrentDataForLayout = gql`
  query CurrentDataForLayout($chatid: ID!){
  	viewer{
        chatroom(friend_id:$chatid,roomtype:"support"){
          id
          member{
            id
            name
          }
          guest{
            name
            email
          }
          messages(first:100){
            edges{
              node{
                id
                sender
                timestamp
                message
                filename
                path
                mimetype
                readby
              }
            }
          }
        }
    }
    	}
`;


function GetUserRoom () {
	if(JSON.parse(localStorage.getItem("clientinfo"))){
      return JSON.parse(localStorage.getItem("clientinfo")).roomid;
    }
    return "";	
}

export default graphql(
	CurrentDataForLayout, 
	)(Chat);
