import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import {host_ip,port_app} from '../components/constant'
import moment from 'moment'
import { browserHistory } from 'react-router'

var socket = require('socket.io-client')(host_ip+port_app);

//import GroupList from '../components/Main/GroupList'
import UserList from '../components/Main/UserList'
import SupportList from '../components/PlugIn/SupportList'
import MenuIcon from '../components/Main/MenuIcon'
import ChatBackend from './ChatBackend'

/*import ArchiveList from '../components/Main/ArchiveList'
import ChatNotificationList from '../components/Main/ChatNotificationList'
import MainContent from './MainContent'


import SubjectList from '../components/Main/SubjectList'
*/ 

export default class HomePage extends Component {

		constructor(props) {
		super(props);
			this.state = {
			openMode: "friends",
			chatid:'5888c54373839eae498bf984',
			roomtype:'direct'
			};
		}

		openUserList(){
			this.setState({openMode: 'friends'});
		}
		
		openSupportChat(){
			this.setState({openMode: 'support'});
		}

		/*openChatList() {
			this.setState({openMode: 'notification'});
		}

		openArchiveList() {
			this.setState({openMode: 'archive'});
		}

		*/

		getMe(){
		if(JSON.parse(localStorage.getItem("userinfo"))){
			return JSON.parse(localStorage.getItem("userinfo")).userid;
		}
		return "5888c54373839eae498bf984";
		}

		/*openSubjectList() {
			this.setState({openMode: 'subject'});
		}*/

		/*updateData() {
				this.props.relay.forceFetch({
					my_id: this.getMe()
			});
		}*/

		/*getMsgNotRead() {
			let unread = 0;
			if (this.props.data.alltotalnotread != undefined) {
			this.props.data.alltotalnotread.map((data) => {
				unread = unread + data.messages.edges.length;
				});
			}
			return unread;
		}

		getClass() {
			if (this.getMsgNotRead() == 0) {
			return "button__badge hide";	
			}
			return "button__badge";
		}*/

		getComponent() {
			switch (this.state.openMode) {
			case "friends":
				return <UserList data={this.props.data} params={this.props.params}/>
			case "notification":
				return <ChatNotificationList data={this.props.data} params={this.props.params}/>
			case "archive":
				return <ArchiveList data={this.props.data} params={this.props.params}/>
			case "subject":
				return <SubjectList data={this.props.data} params={this.props.params}/>
				}
		}

		componentDidMount() {
			//this.updateData();
		}

		_toDirectChat(roomid){
			this.setState({ //the error happens here
					chatid: roomid,
					roomtype:'direct'
				});	
		}

		

		
		

	
		render() {
		
			return (
		    <div className="main-content-backend">
				<div className="main-content-container columns is-mobile"> 
					<div className="left-panel">
						<MenuIcon icon_name="person" tooltip="Friends" onClick={this.openUserList.bind(this)} notification={0} />
					  <MenuIcon icon_name="headset_mic" tooltip="Support" onClick={this.openSupportChat.bind(this)} notification={0} />
						{/*<i
							className="material-icons icon tooltip"
							onClick={this.openUserList.bind(this)}>&#xE7EF;
							<span className="tooltiptext">Friends</span>
						</i>	
						<div className="button">
						<i
							className="material-icons icon tooltip"
							onClick={this.openChatList.bind(this)}>&#xE0C9;
							<span className="tooltiptext">Chat</span>
						</i>
						<span className={this.getClass()}>{this.getMsgNotRead()}</span>
						</div>
						<div className="button">
						<i
							className="material-icons icon tooltip"
							onClick={this.openChatList.bind(this)}>headset_mic
							<span className="tooltiptext">Chat</span>
						</i>
						<span className={this.getClass()}>{this.getMsgNotRead()}</span>
						</div>
						<div className="button">
						<i
							className="material-icons icon tooltip"
							onClick={this.openArchiveList.bind(this)}>archive
							<span className="tooltiptext">Archive</span>
						</i>
						</div>
						<div className="button">
							<i
							className="material-icons icon tooltip"
							onClick={this.openSubjectList.bind(this)}>assignment
							<span className="tooltiptext">Subject</span>
						</i>
						</div> */}
					</div>
					<div className="main-content-chat"> 
						<div className="left-panel column is-one-quarter">
								{this.props.params.roomtype == "support" ? (
					        <SupportList id={this.getMe()} _toDirectChatSupportChild={this._toDirectChat.bind(this)}/>
						      ) : (
						      <UserList id={this.getMe()} _toDirectChatChild={this._toDirectChat.bind(this)} />)
								}
						</div>			
						<div className="right-panel column">
								<ChatBackend my_id='5888c54373839eae498bf984' friend_id={this.state.chatid} roomtype={this.state.roomtype}/>
						</div>		
					</div>
					</div>			
				</div>
				);
		}
}

/*const CurrentDataForLayout = gql`
  query CurrentDataForLayout($chatid: ID!){
  	viewer{
        chatroom(friend_id:$chatid,roomtype:"support"){
          id
          member{
            id
            name
          }
          guest{
            name
            email
          }
          messages(first:100){
            edges{
              node{
                id
                sender
                timestamp
                message
                filename
                path
                mimetype
                readby
              }
            }
          }
        }
    }
	}
`;


function GetUserRoom () {
	if(JSON.parse(localStorage.getItem("clientinfo"))){
      return JSON.parse(localStorage.getItem("clientinfo")).roomid;
    }
    return "";	
}

export default graphql(
	CurrentDataForLayout, 
	)(HomePage);
*/

/*export default Relay.createContainer(HomePage, {
		initialVariables: {
				my_id: null
		},
		fragments: {
				data: () => Relay
						.QL `
      fragment on Viewer {
        ${UserList.getFragment('data')},
        ${GroupList.getFragment('data')},
        ${ChatNotificationList.getFragment('data')},
		${ArchiveList.getFragment('data')},
		${SubjectList.getFragment('data')},
		${SupportList.getFragment('data')},
		alltotalnotread(my_id:$my_id) {
			      id
			      messages(first: 100) {
			        edges {
			          node {
			            id
			            sender
			            timestamp
			            message
			            filename
			            path
			            mimetype
			            readby
			          }
			        }
			      }
			    } 
      }
    `
		}
});*/