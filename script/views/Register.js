import React, { Component } from 'react';
import fetch from 'isomorphic-fetch'
import {host_ip,port_app,appid} from '../components/constant'
import Plugin from './Plugin'

export default class Register extends Component {
	start(){
		fetch(host_ip+port_app+"/plug/client",{
			method: 'post',
			headers: {
		        'Content-Type':"application/json"
		    },
			body:JSON.stringify({
				"email":this.refs.input_email.value,
				"name":this.refs.input_name.value,
			})
		})
		.then(response => response.json())
		.then(json => {  	
		  	localStorage.setItem("clientinfo",JSON.stringify(json));
		  	localStorage.setItem("subject",this.refs.input_subject.value);
		  	localStorage.setItem("chatid",json.roomid);
		  	this.props.success(json.guestid,this.refs.input_subject.value,json.roomid);
		})

	}

	render() {
		if(appid=="CEDAR"||appid=="IDYL"){
			return (<Plugin success={this.props.success} appid={appid}/>)
		}

		return (
			<div className="chat-form">
				<div className="field-container">
					<div className="label">Name / ชื่อ:</div>
					<div className="input-container">
						<input ref="input_name" type="text" />
					</div>
				</div>
				<div className="field-container">
					<div className="label">Email / อีเมล์:</div>
					<div className="input-container">
						<input ref="input_email" type="text" />
					</div>
				</div>
				<div className="field-container">
					<div className="label">Subject / เรื่อง:</div>
					<select ref="input_subject">
						<option value="1">คอร์สอบรม</option>
						<option value="2">Idyl Support</option>
						<option value="3">Consult</option>
						<option value="4">Sales</option>
					</select>
				</div>
				<button className="mdl-button mdl-js-button mdl-button--raised" onClick={this.start.bind(this)}>
				  เริ่มใช้งาน
				</button>
			</div>
		);
	}
}
