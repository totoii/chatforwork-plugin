
import React, { Component } from 'react';
import Relay from 'react-relay'
import Header from '../components/Main/Header'
import HeaderBackend from '../components/Main/HeaderBackend'
import MainContent from './MainContent'
import {frontdesk} from '../components/constant'

export default class ContainerChat extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	is_open: false,
	    };

  	}

	render() {

		return (
			frontdesk?
			(
				<div className="chat-plugin-main">	
					<Header is_open={this.state.is_open} toggle_open={this.toggle_open.bind(this)}/>
					{
						this.state.is_open?<MainContent/>:null
					}
				</div>
			):
			(
				
				<div className="chat-plugin-main">	
					<HeaderBackend is_open={this.state.is_open} toggle_open={this.toggle_open.bind(this)}/>
					{
						this.state.is_open?<MainContent/>:null
					}
				</div>
			)
			
		);
	}

	toggle_open(){
		this.setState({
			is_open:!this.state.is_open
		})
	}
}