import 'babel-polyfill'
import './css/import.less';
// import '../node_modules/material-design-lite/material.min.js';

import React from 'react'
// import Relay from 'react-relay'
import ReactDOM from 'react-dom'
import { Router, Route, browserHistory, applyRouterMiddleware,IndexRoute} from 'react-router'
import ChatBackend from './views/ChatBackend'
// import useRelay from 'react-router-relay'

import Container from './views/Container'
import ContainerChat from './views/ContainerChat'

import ApolloClient, { createNetworkInterface } from 'apollo-client';
import {
  host_ip,
  port_graph,
} from './components/constant'

const client = new ApolloClient({
  networkInterface: createNetworkInterface({ uri: host_ip+port_graph}),
});

import { ApolloProvider } from 'react-apollo';



// const socket = io.connect('https://localhost:1999');

// var socket = require('socket.io-client')('https://localhost:1999', {secure: true});

var head = document.getElementsByTagName('head')[0];
var body = document.getElementsByTagName('body')[0];

var link  = document.createElement('link');
link.rel  = 'stylesheet';
link.type = 'text/css';
link.href = 'https://fonts.googleapis.com/icon?family=Material+Icons';
head.appendChild(link);

// 	link  = document.createElement('link');
//     link.rel  = 'stylesheet';
//     link.type = 'text/css';
//     link.href = '/chatforwork-plugin.css';
// 	head.appendChild(link);

link  = document.createElement('link');
link.rel  = 'stylesheet';
link.type = 'text/css';
link.href = 'https://fonts.googleapis.com/css?family=Roboto';
head.appendChild(link);

link  = document.createElement('link');
link.rel  = 'stylesheet';
link.type = 'text/css';
link.href = 'https://code.getmdl.io/1.3.0/material.indigo-pink.min.css';
head.appendChild(link);

link  = document.createElement('link');
link.type = 'text/javascript';
link.href = 'https://code.getmdl.io/1.3.0/material.min.js';
head.appendChild(link);

var ChatDiv = document.createElement('div');
body.appendChild(ChatDiv);

class Wrapper extends React.Component {
  render() {
    return (
      <div className="chat-plugin-default-container">
	      <ApolloProvider client={client}>
         <Router history={browserHistory}>
           <Route path='/' component={Container}>
               <Route path='/:roomtype' component={Container}/>
               <Route path='/:roomtype/:roomid' component={Container}/>
           </Route>
        </Router>
		    
		  </ApolloProvider>
      </div>
    );
  }
}


module.exports.render = ()=> {
  ReactDOM.render(
    <Wrapper />
    , ChatDiv
  )
}