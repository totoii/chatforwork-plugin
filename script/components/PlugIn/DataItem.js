import React, { Component } from 'react';
import{Link} from 'react-router'
import { browserHistory } from 'react-router'
export default class DataItem extends Component {

	getClass(){
		if(this.props.selected_id==this.props.data_id){
			return "data-item selected"; 
		}
		
		return "data-item";
	}

	_toSupportChat(){
		this.props._toDirectChatSupportChild(this.props.data_id)
		//browserHistory.push('/support/' + this.props.data_id);
	}
	render() {
		return (
			<div className={this.getClass()} onClick={this._toSupportChat.bind(this)}>
				<div>
					{this.props.data}
				</div>
				<div className="online-status">
					<span className="online-icon"></span>
				</div>	
					{/*<Link className={this.getClass()} to={this.props.to}>
				<div>
					{this.props.data.name} ({this.props.data.email})
				</div>
				{
					this.props.noticount>0?(<div className="notification">{this.props.noticount}</div>):null
				}				
			</Link>
			*/}
			</div>
		
		);
	}
}