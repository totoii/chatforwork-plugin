import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

import DataItem from './DataItem'

//var socket = require('socket.io-client')();
import {host_ip,port_app} from '../constant'

var socket = require('socket.io-client')(host_ip+port_app);


class SupportList extends Component {
	// _load(){
	// 	this.props.relay.setVariables({count:1});
	// }

	componentWillMount() {
		// if(!this.props.params.roomtype){
		// 	if(this.props.data.friends.edges.length>0){
		// 		browserHistory.push('/direct/'+this.props.data.friends.edges[0].node.id);
		// 	}
		// }
	}

	componentDidMount(){
		this.props.data.refetch();
	}

	/*componentDidMount() {
		socket.emit('join_in',getCurrID());
		socket.on('data_received',()=>{
			this.props.relay.forceFetch();
		})
	}*/

	render() {
		let selected = "";
		let myid = getCurrID();
		
		if(this.props.data.viewer == undefined)
			return null;
		{
			console.log(this.props.data.viewer.allsupport)
		}
		// if(this.props.params.roomtype){
		// 	if(this.props.params.roomtype=="direct"){
		// 		selected=this.props.params.roomid;
		// 	}
		// }

		{/*let RoomList = this.props.data.supports.edges.map(({node})=>{
			let counter = 0;

			node.messages.edges.map((data)=>{
				if(data.node.readby.indexOf(myid)<0){
					counter++;
				}
			})

			return (<DataItem key={node.id} selected_id={selected} noticount={counter} data_id={node.id} to={`/support/${node.id}`} data={node.guest} />);
		});

		return (
			<div>
				<div className="label">Client</div>
				{RoomList}
			</div>
		);
	*/}
		return (
			<div>
				<div className="label">Support</div>
				{
					this.props.data.viewer.allsupport.map((data)=>
						(<DataItem key={data.id} data_id={data.id} to={`/direct/${data.id}`} data={data.name} _toDirectChatSupportChild={this.props._toDirectChatSupportChild}  />)
					)
				}	
			</div>
		)
	}
}

function getCurrID(){
    if(JSON.parse(localStorage.getItem("userinfo"))){
      return JSON.parse(localStorage.getItem("userinfo")).userid;
    }
    return "";
}

const CurrentDataForLayout = gql`
  query CurrentDataForLayout{
  	viewer{
		allsupport(grouptype:"support"){
			id,
			name
			}
		}
	}
`;


function GetUserRoom () {
	if(JSON.parse(localStorage.getItem("clientinfo"))){
      return JSON.parse(localStorage.getItem("clientinfo")).roomid;
    }
    return "";	
}

export default graphql(
	CurrentDataForLayout, 
	)(SupportList);
