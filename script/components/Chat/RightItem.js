import React, { Component } from 'react';
import moment from 'moment'

export default class RightItem extends Component {
	render() {

		if(this.props.data.filename != null){
			return (<RightItem_File roomtype={this.props.roomtype} data={this.props.data}/>)
		}

		return (
			<div className="balloon-right-wrapper">
				<div className="inner-container">
					<Item_Info roomtype={this.props.roomtype} readby={this.props.data.readby} timestamp={this.props.data.timestamp} me={this.props.me}/>
					<div className="balloon-right">
						{this.props.data.message}
					</div>
				</div>
			</div>
		);
	}
}

class RightItem_File extends Component {
	render() {
		let CONTENT = (
				<div className="ballon-img">
					<img src={ "/"+this.props.data.path} />
				</div>);

		if(this.props.data.mimetype.indexOf('image')<0){
			var filename = this.props.data.path;
			filename = filename.substring(filename.indexOf('/')+1);
			if(filename==""){
				filename="file";
			}
			CONTENT = (
			<div className="balloon-right">
				<a href={"/files/"+this.props.data.path} download>{this.props.data.filename}</a>
			</div>);
		}

		return (
			<div className="balloon-right-wrapper">
				<div className="inner-container">
					<Item_Info roomtype={this.props.roomtype} readby={this.props.data.readby}  timestamp={this.props.data.timestamp}/>
					<div className="balloon-right img">
						{CONTENT}
					</div>
				</div>
			</div>
		);
	}
}

class Item_Info extends Component {
	render() {
		let is_read = this.props.readby.length-1>0
		let READ_TAG = "";
		if(this.props.roomtype=="direct"){
			READ_TAG = is_read?(<div>Read</div>):null
		}else{
			READ_TAG = is_read?(<div className="tooltip">Read By {this.props.readby.length-1} <span className="tooltiptext">{this.getMemberRead()}</span> </div>):null
		}

		return (
			<div className="info">
				{READ_TAG}				
				<div>{moment(new Date(this.props.timestamp)).format('HH:mm')}</div>
			</div>
		);
	}
	getMemberRead(){
		let membername = '';
		for (var i = 0; i < this.props.readby.length; i++) { 
			if(this.props.readby[i].id != this.props.me)
				membername += this.props.readby[i].name + "\n";
		}
		return membername;
	}
}
