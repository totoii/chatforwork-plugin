import React, { Component } from 'react';
import LeftItem from './LeftItem';
import RightItem from './RightItem'

// let socket = io();

export default class ChatList extends Component {

	 componentDidMount() {
	 	this._toBottom();
	    // socket.on('msg_received', ()=>{
	    // 	this._toBottom();
	    // });
	}

	componentDidUpdate(){
		console.log(this.props.msglist);
		// console.log("update");
		document.getElementsByClassName("chat-container")[0].scrollTop=5000;
	}

	_toBottom(){
		document.getElementsByClassName("chat-container")[0].scrollTop=5000;
			   // /console.log(container.scrollHeight);
	}

	render() {
		

		return (
			<div className="chat-wrapper">
				<div className="chat-container">
					{this.props.msglist.map(({node})=>{
						if(node.sender==this.props.me){
							return <RightItem key={node.id} roomtype={this.props.roomtype} data={node} me={this.props.me}/>
						}
						else{
							return <LeftItem key={node.id} roomtype={this.props.roomtype} user_dic={this.props.user_dic} data={node}/>
						}
					})}
				</div>
			</div>
		);
	}
}
