import React, { Component } from 'react';
import Relay from 'react-relay';
import { browserHistory } from 'react-router'
import AddUserItem from './AddUserItem'

export default class FriendList extends Component {
	componentDidUpdate() {
		console.log(this.props.userlist)
	}
	componentDidMount() {
    	console.log(this.props.userlist)
  	}

	render() {
		return (
			<div>
				<div className="label">Friends</div>
				{
					this.props.userlist.map((data) => {
						return(<AddUserItem key={data.id} dataid={data.id} name={data.name} isNewMember={this.props.isNewMember}></AddUserItem>)
					})
				}
				
				<div className="button">
					<div className="btn-wrap">
						<button className="btn-cancel" onClick={this.props.cancelFriendToChatroom}>
							Cancel
						</button>

						<button className="btn-add" onClick={this.addNewMember.bind(this)}>
							Add People
						</button>
					</div>
				</div>
				
			</div>
			)
	}

	getMe(){
	  if(JSON.parse(localStorage.getItem("userinfo"))){
	      return JSON.parse(localStorage.getItem("userinfo")).userid;
	  }
	    return "";
	  }
	addNewMember(){
		 if(this.props.isNewMember.length > 0){
	      this.props.socket.emit('inviting_member',this.props.isNewMember,this.props.roomid, this.getMe());
	    }
	    this.props.addNewMemberToChatroom();
	}
}