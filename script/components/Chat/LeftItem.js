import React, { Component } from 'react';
import moment from 'moment'

export default class LeftItem extends Component {
	render() {
		if(this.props.data.filename != null){
			return (<LeftItem_File data={this.props.data} roomtype={this.props.roomtype} sender={this.props.user_dic[this.props.data.sender]}/>)
		}

		return (
			<div className="balloon-wraper">
				<Item_Sender sender={this.props.user_dic[this.props.data.sender]} roomtype={this.props.roomtype}/>
				<div className="inner-container">
					<div className="balloon-left">
						{this.props.data.message}
					</div>
					<Item_Info timestamp={this.props.data.timestamp}/>
				</div>
			</div>
		);
	}
}

class LeftItem_File extends Component {
	render() {

		let CONTENT = (
				<div className="ballon-img">
					<img src={ "/"+this.props.data.path} />
				</div>);

		if(this.props.data.mimetype.indexOf('image')<0){
			var filename = this.props.path;
			if(filename){
				filename = filename.substring(filename.indexOf('/')+1);
				if(filename==""){
					filename="file";
				}
			}else{
				filename="file";
			}

			CONTENT = (
			<div className="chat-ballon">
				<a href={"/"+this.props.data.path} download>{filename}</a>
			</div>);
		}

		return (
			<div className="balloon-wraper">
				<Item_Sender sender={this.props.sender} roomtype={this.props.roomtype}/>
				<div className="inner-container">
					<div className="balloon-left">
						{CONTENT}
					</div>
					<Item_Info timestamp={this.props.data.timestamp}/>
				</div>
			</div>
		);
	}
}

class Item_Info extends Component {
	render() {
		return (
			<div className="info">
				<div>{moment(new Date(this.props.timestamp)).format('HH:mm')}</div>
			</div>
		);
	}
}

class Item_Sender extends Component {
	render() {
		if(this.props.roomtype=="direct"){
			return null
		}

		return (
			<div className="sender">
				{this.props.sender}
			</div>
		);
	}
}