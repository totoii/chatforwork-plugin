import React, {Component} from 'react';
import Calendar from 'react-datetime';
import TokenInput  from 'react-tokeninput';
import $ from 'jquery';


export default class AddSubject extends Component {
	constructor(props) {
    super(props);
    this.state={
      source: [],
      duedate: null
    }
  }

  
  componentDidMount() {
    var userlist = []
    this.props.allusers.map((data, index) => {
      let user = {value: data.id, label: data.name}
      this.setState({source: this.state.source.push(user)}) 
      userlist.push(user)
    })
  
    this.renderBuyButton();
  }
  

  renderBuyButton() {
   
  /* var engine = new Bloodhound({
    local: [{value: 'red'}, {value: 'blue'}, {value: 'green'} , {value: 'yellow'}, {value: 'violet'}, {value: 'brown'}, {value: 'purple'}, {value: 'black'}, {value: 'white'}],
    datumTokenizer: function(d) {
      return Bloodhound.tokenizers.whitespace(d.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace
  });

  engine.initialize();*/
  this.$node = $(this.refs.txtAssginto);
  this.$node.tokenfield({
      autocomplete: {
        source: this.state.source,
        delay: 100,
        autoFocus: true
      },
      showAutocompleteOnFocus: true,
    })

   /* this.$node = $(this.refs.txtAssginto);
    
    this.$node.tokenfield({
      autocomplete: {
        source: this.state.source,
        delay: 100
      },
      showAutocompleteOnFocus: true
    })*/
  }

	render() {
		return (
			
          <div className="add-subject">
            <h2>
              {this.props.isNewSubject?"New Subject":"Edit Subject"}
            </h2>
            <div className="columns is-multiline is-mobile">
            <div className="column is-10">
              <label className="label-input">Subject</label>
              <p className="control">
                <input className="input"
                id="subject" 
                placeholder="Write Subject..." 
                ref="txtSubject"/>
            </p>  
            </div>
            
            <div className="column is-10">
              <label className="label-input">Due date</label>
              <Calendar 
              dateFormat="DD MMM YYYY" 
              timeFormat={false}
              ref="txtDuedate"
              closeOnSelect={true}
              onChange={this._changeDueDate.bind(this)}/>
            </div>
            <div className="column is-10">
              <label className="label-input">Assign to</label>
              <p className="control">
              <input type="text" className="form-control" ref="txtAssginto" id="tokenfield"  />
              </p>
            </div>
            <div className="column is-10">
              <label className="label-input">Remark</label>
              <p className="control">
              <textarea name="Text1" cols="40" rows="5" ref="txtRemark"></textarea>
               </p>
            </div>
            <div className="column is-10">
            <div className="button">
              <div className="btn-wrap">
                <button className="btn-cancel" onClick={this.props.cancelSubjectToChatroom}>
                  Cancel
                </button>
                <button className="btn-add" onClick={this.saveSubject.bind(this)}>
                  Save
                </button>
              </div>
            </div>
          </div>
			    </div>
        </div>
			)			
	}
  getMe(){  
	if(JSON.parse(localStorage.getItem("userinfo"))){
	      return JSON.parse(localStorage.getItem("userinfo")).userid;
	}
	    return "";
	}
	_changeDueDate(e){
    this.setState({duedate: e.toDate()}) 
	}

  saveSubject(){

    let res = this.refs.txtAssginto.value.split(",");
    
    let assignto = []
    if(res.length > 1)
    {
       res.map((data, index) => {
          assignto.push(data.trim());
       })
    }
    else if(res.length == 1){
       assignto.push(res[0]);
    }
    
    this.props.socket.emit('addsubject',this.refs.txtSubject.value, this.props.roomid, assignto, this.getMe(), this.state.duedate, this.refs.txtRemark.value);
    this.props.saveSubjectToChatroom();
   
	}	
}