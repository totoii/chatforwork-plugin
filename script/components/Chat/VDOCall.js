import React, { Component } from 'react';

export default class VDOCall extends Component {
	render() {
		return (
			<div>
				{
					this.props.isCalling?
					<Calling
						callWithVideo={this.props.callWithVideo}
						caller={this.props.caller}
						/>:null
				}
				{
					this.props.callActive?
					<Vdo 
						peerSrc={this.props.peerSrc} 
						localSrc={this.props.localSrc}/>
						:null
				}
			</div>
		);
	}
}


class Calling extends Component {
	render() {
		return (
			<div className="chat-call">
				<div className="signal" />
				incoming call...
				<button onClick={this.props.callWithVideo.bind(this,false,this.props.caller)}>Accept</button>
			</div>
		);
	}
}

class Vdo extends Component{
	render(){
		return(
			<div className="chat-call">
				<video id="peerVideo"  src={this.props.peerSrc} autoPlay></video>
				<video id="localVideo"  src={this.props.localSrc} autoPlay muted></video>
			</div>
			)
	}
}