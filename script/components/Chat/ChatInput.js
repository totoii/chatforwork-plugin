import React, { Component } from 'react';

// let socket = io();

export default class ChatInput extends Component {

	
	_press(e){
		if(e.key=="Enter"){
			if(e.target.value.trim()!=""){
				this.props.socket.emit('msg_sended',e.target.value,this.props.roomid,this.props.me);
				e.target.value = "";
			}
			// console.log(this.props.roomid);
		}
	}
	openUpload(){
		this.upload.click()
	}
	
	onFileUploaded(e){

		var form = new FormData() 
        form.append('file', e.target.files[0])
        form.append('roomid', this.props.roomid)
        form.append('my_id', this.props.me)

        fetch('/api/file/upload', 
        	{
                method: 'POST',
                body: form
            })
            .then(function(res) {
                return res.json()
            })
            .then(function(json) {
            	this.props.socket.emit('file_sended', json, this.props.roomid, this.props.me);
            }.bind(this))
            
	}

	render() {
		return (
			<div className="chat-input"> 
				<input placeholder="Type a message..." onKeyPress={this._press.bind(this)}/>

				<div className="attach">
					<i className="material-icons" onClick={this.openUpload.bind(this)}>attach_file</i>
					<input
						type="file"
						name="chat_file"
						ref={node=>{this.upload = node}}
						onChange={this.onFileUploaded.bind(this)}
						/>
				</div>
			</div>
		);
	}
}
