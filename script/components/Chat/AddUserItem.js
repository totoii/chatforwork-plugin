import React, { Component } from 'react';
import{Link} from 'react-router'
export default class AddUserItem extends Component {

	getClass(checked){
		if(checked){
			return "member-item selected"; 
		}
		
		return "member-item";
	}

	render() {
		return (
			<div>
		        <div className="member-item">
		            <div className="member-checkbox">
		            <input type="checkbox" onClick={this.handleOnClick.bind(this)} name={this.props.name}  id={this.props.dataid}/>
		            </div>
		             {this.props.name}
		        </div>
	        </div>
		);
	}

	handleOnClick(event){
	  if(!event.target.checked){
	  	this.props.isNewMember.splice(this.props.isNewMember.indexOf(event.target.id),1);
	  }
	  else{
	  	this.props.isNewMember.push(event.target.id);
	  }
  }
}