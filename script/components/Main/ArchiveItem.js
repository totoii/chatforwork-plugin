import React, { Component } from 'react';
import{Link} from 'react-router'

export default class ArchiveItem extends Component {

	getClass(){
		if(this.props.selected_id==this.props.data_id){
			return "data-item selected"; 
		}
		
		return "data-item";
	}

	getMsgNotRead(roomid){
		let unread = 0;
  		if(this.props.unread != undefined){
  			this.props.unread.map((data)=>{  
	        	if(data.id == roomid){
	        		 unread = data.messages.edges.length;
	        	}
	      });
  		}
  		return unread;
  	}

  	getLastMsg(){
  		let msg = "" 
  		if(this.props.data.messages != undefined){
  			this.props.data.messages.edges.map(({node})=>{  
  				console.log(node.message);
	        	if(node.message != undefined && msg == ""){
	        		msg = node.message;
	        	}
	      });
  		}
  		return msg;
  	}

	clickUnArchive(){
		this.props.updateunarchive(this.props.data_id, false);
	}  

	render() {
		return (
			<div>
				{this.getLastMsg() != "" ?
				(<div className="display-flex">
					<Link className="data-item" to={this.props.to}>
						<div>
							{this.getLastMsg()}
						</div>
					</Link>
					<i className="material-icons cursor-pointer" onClick={this.clickUnArchive.bind(this)}>unarchive</i>
				</div>): null
				}
			</div>
		)	
	}

	
}