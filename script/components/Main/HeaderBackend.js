import React, { Component } from 'react';

export default class Header extends Component {
	render() {
		let head_style= this.props.is_open?"chat-plugin-header turn-on-backend":"chat-plugin-header turn-off";

		return (
			<div className={head_style} onClick={this.props.toggle_open}>
				PA Live Chat
				<i className="material-icons">expand_more</i>
			</div>
		);
	}
}
