import React, {Component} from 'react';
import Relay from 'react-relay';
import {browserHistory} from 'react-router'
import SubjectItem from './SubjectItem'

class SubjectList extends Component {
	constructor(props) {
        super(props);
		this.state={
      	mode: "all",
   		}
        // Bind functions
        //this.handleClick = this.handleClick.bind(this);
        //this.handleSubmit = this.handleSubmit.bind(this);
    }

	getMe() {
		if (JSON.parse(localStorage.getItem("userinfo"))) {
			return JSON
			.parse(localStorage.getItem("userinfo"))
			.userid;
		}
			return "";
	}

	updateData() {
		this.props.relay.forceFetch({
			my_id: this.getMe()
		});
	}
	
	componentDidMount() {
	  this.updateData();
	  //socket.on('updateArchivedList', this.updateArchivedList.bind(this));
	}

	/*getComponent() {
		switch (this.state.mode) {
			case "all":
				return <UserList data={this.props.data} params={this.props.params}/>
			case "notification":
				return <ChatNotificationList data={this.props.data} params={this.props.params}/>
			case "archive":
				return <ArchiveList data={this.props.data} params={this.props.params}/>
			case "subject":
				return <SubjectList data={this.props.data} params={this.props.params}/>
		}
	}*/

	filterSubjectByMode(modesubject) {
		this.setState({mode:modesubject});
		
		// Declare all variables
		/*ar i, tabcontent, tablinks;

		// Get all elements with class="tabcontent" and hide them
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}

		// Get all elements with class="tablinks" and remove the class "active"
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}

		
		// Show the current tab, and add an "active" class to the link that opened the tab
		document.getElementById(cityName).style.display = "block";
		evt.target.className += " active";*/
	}
    render() {
        return (
			<div>
            <div className="label">Subject</div>
			<div>
				<div className="tab">
					<a href="javascript:void(0)" className="tablinks" onClick={this.filterSubjectByMode.bind(this, 'all')}>All</a>
					<a href="javascript:void(0)" className="tablinks" onClick={this.filterSubjectByMode.bind(this, 'whowait')}>Who Wait</a>
					<a href="javascript:void(0)" className="tablinks" onClick={this.filterSubjectByMode.bind(this, 'waitwho')}>Wait Who</a>
				</div>
			</div>
			<div>
			{this.props.data.allsubject.map((data) => {
				return (
				<div key={data.id}>
					<SubjectItem
					key={data.id}
					data_id={data.id}
					to={`/direct/${data.id}`}
					data={data}
					mode={this.state.mode}
					createdby={data.createdby}
					assignto={data.assignto}
					/>
				</div>
				)
				})
			}
			</div>
			</div>
        )
    }
}

export default Relay.createContainer(SubjectList, {
    initialVariables: {
        my_id: null
    },
    fragments: {
        data: () => Relay.QL `
    		fragment on Viewer {

			allsubject(my_id:$my_id){	
                id,
                name,
				createdby {
					id,
					name
				}
				assignto {
					id,
					name
				}
	        }
	      }
          
    	`
    }
});