import React, { Component } from 'react';
import Relay from 'react-relay';
import { browserHistory } from 'react-router'
import CreateArchiveMutation from '../../mutations/CreateArchiveMutation';


import ArchiveItem from './ArchiveItem'

const socket = require('socket.io-client')();

class ArchiveList extends Component {
	getMe(){
	   if(JSON.parse(localStorage.getItem("userinfo"))){
	     return JSON.parse(localStorage.getItem("userinfo")).userid;
	   }
	   return "";
	}
	
	updateData(){
    	this.props.relay.forceFetch({
      	 my_id:this.getMe(),
				 archive: true
    	});
  }

	updateUnArchive(roomid, archive){
		socket.emit('archived', roomid, archive);
		/*this.props.relay.commitUpdate(
      		new CreateArchiveMutation({
						roomid:roomid,
						archive:archive,
						viewer:this.props.data
					})
    	);*/
			//window.location.href = '/';
	}

	updateArchivedList(){
		this.updateData();
	}

	componentDidMount() {
	  this.updateData();
		socket.on('updateArchivedList', this.updateArchivedList.bind(this));
	}

	getRoomName(){
    if(this.props.params.roomtype=="direct"){
		if(this.props.data.chatnotification.member.length==0)return "";
	     if(this.props.data.chatnotification.member[0].id!=this.getMe()){
	      return this.props.data.chatnotification.member[0].name;
	     }
     	  return this.props.data.chatnotification.member[1].name;
	    }/*else if(this.props.params.roomtype=="group"){
	      var group_dic = new Object();
	      this.props.data.allgroups.map((data)=>{  
	        group_dic[data.id]=data.name;
	      });

	      return group_dic[this.props.params.roomid];
	    }*/
  	} 	

	render() {
		
		return (
			<div>
				<div className="label">Archive</div>
				<div>
					  {this.props.data.chatnotification.map((data)=>{  
						 	return(
						 		<div key={data.id}>
							 		<ArchiveItem 
									 key={data.id} 
									 data_id={data.id} 
									 to={`/direct/${data.id}`} 
									 data={data} 
									 updateunarchive={this.updateUnArchive.bind(this)}  />
								</div>
							)	
					 	})
					 } 
				</div>
					
			</div>
		);
	}
}

export default Relay.createContainer(ArchiveList, {
    initialVariables: {
      my_id:null,
			archive: false
    },
  	fragments: {
    	data: () => Relay.QL`
    		fragment on Viewer {

       chatnotification(my_id:$my_id, archive: $archive){
           id,
					 archive
				member{
					id
					name
				}
				messages(first:100){
					edges{
					  node{
					    id
					    sender
					    timestamp
					    message
					    filename
					    path
					    mimetype
					    readby
					  }
					}
	            }
	        }

	        alltotalnotread(my_id:$my_id) {
			      id
			      messages(first: 100) {
			        edges {
			          node {
			            id
			            sender
			            timestamp
			            message
			            filename
			            path
			            mimetype
			            readby
			          }
			        }
			      }
			    }
	      }
          
    	`,
  	},
});
