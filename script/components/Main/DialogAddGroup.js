import React, { Component } from 'react';
import Dialog from 'react-toolbox/lib/dialog';

export default class DialogAddGroup extends Component {
	state = {
	    active: this.props.active
	};

	handleToggle = () => {
	    this.setState({active: !this.state.active});
	}

	actions = [
	    { label: "Cancel", onClick: this.handleToggle },
	    { label: "Save", onClick: this.handleToggle }
	];

	render() {
		return (
			<Dialog
	          actions={this.actions}
	          active={this.state.active}
	          onEscKeyDown={this.handleToggle}
	          onOverlayClick={this.handleToggle}
	          title='My awesome dialog'>

	          <p>Here you can add arbitrary content. Components like Pickers are using dialogs now.</p>

        	</Dialog>
		);
	}
}
