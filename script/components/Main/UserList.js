import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import {host_ip,port_app} from '../constant'

import DataItem from './DataItem'

class UserList extends Component {
	// _load(){
	// 	this.props.relay.setVariables({count:1});
	// }

	/*componentWillMount() {
		if(!this.props.params.roomtype){
			if(this.props.data.friends.edges.length>0){
				browserHistory.push('/direct/'+this.props.data.friends.edges[0].node.id);
			}
		}
	}*/

	componentDidMount(){
		this.props.data.refetch();
	}

	render() {
		if(this.props.data.viewer == undefined)
			return null;
		/*let selected = "";
		if(this.props.params.roomtype){
			if(this.props.params.roomtype=="direct"){
				selected=this.props.params.roomid;
			}
		}*/
		return (
			<div>
				<div className="label">Friends</div>
				{
					this.props.data.viewer.friends.edges.map(({node})=>
						(<DataItem key={node.id} data_id={node.id} to={`/direct/${node.id}`} data={node.name} _toDirectChatChild={this.props._toDirectChatChild} />)
					)
				}	
			</div>
		);
	}
}

const CurrentDataForLayout = gql`
  query CurrentDataForLayout($id: ID!){
  	viewer{
		friends(id:$id,first:200){
			edges {
		    	node {
		      		id
		      		name
		      		}
		      	}
			}  
    	}
	}
`;


function GetUserRoom () {
	if(JSON.parse(localStorage.getItem("clientinfo"))){
      return JSON.parse(localStorage.getItem("clientinfo")).roomid;
    }
    return "";	
}

export default graphql(
	CurrentDataForLayout, 
	)(UserList);
