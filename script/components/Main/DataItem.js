import React, { Component } from 'react';
import { browserHistory } from 'react-router'
export default class DataItem extends Component {

	getClass(){
		/*if(this.props.selected_id==this.props.data_id){
			return "data-item selected"; 
		}*/
		
		return "data-item";
	}

	render() {
		return (
			<div className={this.getClass()} onClick={this._toDirectChat.bind(this)}>
				<div>
					{this.props.data}
				</div>
				<div className="online-status">
					<span className="online-icon"></span>
				</div>	
			</div>
		);
	}

	_toDirectChat(){
		this.props._toDirectChatChild(this.props.data_id)
		//browserHistory.push('/direct/' + this.props.data_id);
	}
}