import React, { Component } from 'react';

export default class MenuIcon extends Component {
	render() {
		return (
			<div className="button">
			    <i className="material-icons icon tooltip" onClick={this.props.onClick}>
			    	{this.props.icon_name}
			    	<span className="tooltiptext">{this.props.tooltip}</span>
			    </i>
		    	<span className={this.getClass()}>{this.props.notification}</span>
		  	</div>
		);
	}

	getClass(){

		if(this.props.notification == 0) {
			return "button__badge hide"; 
		}
		return "button__badge";
	}
}
