import React, { Component } from 'react';
import{Link} from 'react-router'

export default class SubjectItem extends Component {
	getMe() {
		if (JSON.parse(localStorage.getItem("userinfo"))) {
			return JSON
			.parse(localStorage.getItem("userinfo"))
			.userid;
		}
			return "";
	}
	
	getClass(mode, assignto, createdby){
		var cssclass = "data-item"
		if(mode == "whowait"){//whowait
			assignto.map((data) =>{
				if(data.id != this.getMe())
					cssclass= "data-item hide"
			})
		}
		else if(mode == "waitwho"){
			if(createdby.id == this.getMe())
				cssclass = "data-item"
			else 	
				cssclass = "data-item hide"
		}
		else
			cssclass = "data-item";
	
		return cssclass;
	}

	render() {
		return (
			<div>
				<Link className={this.getClass(this.props.mode, this.props.assignto, this.props.createdby)} to={this.props.to}>
					<div>
						{this.props.data.name}
					</div>
				</Link>
			</div>
		)	
	}
}