import React, { Component } from 'react';
import Relay from 'react-relay';
import { browserHistory } from 'react-router'

import DataItem from './DataItem'

class GroupList extends Component {

	_addGroup(e){
		e.preventDefault();
		browserHistory.push('/addGroup')
	}

	render() {
		let selected = "";
		if(this.props.params.roomtype){
			if(this.props.params.roomtype=="group"){
				selected=this.props.params.roomid;
			}
		}

		return (
			<div>
				<div className="label">Groups</div>
				{
					this.props.data.allgroups.map(({id,name})=>
						(<DataItem 
							key={id} 
							to={`/group/${id}`} 
							data={name} 
							data_id={id}
							selected_id={selected}/>)
					)
				}

				<a href="" onClick={this._addGroup}> + Create Group </a>
			</div>
		);
	}
}

export default Relay.createContainer(GroupList, {
  // forchFetch:true,
  fragments: {
    data: () => Relay.QL`
      fragment on Viewer {
        allgroups{
        	id
        	name
        }
      }
    `,
  },
});