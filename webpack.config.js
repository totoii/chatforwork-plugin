var path = require('path');
var webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
 
    // ให้ webpack เริ่มรวมโค้ดที่ไฟล์ client.js
    entry: path.resolve(__dirname, 'script/client.js'),

    // แล้วตั้งชื่อไฟล์ output ว่า bundle.js
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'chatforwork-plugin.js',  
        publicPath: '/',
        library: 'cfw',
        libraryTarget: 'var',
    },
    // plugins: [
    //   new webpack.optimize.OccurenceOrderPlugin(),
    //   new webpack.HotModuleReplacementPlugin(),
    //   new webpack.NoErrorsPlugin(),
    //   new ExtractTextPlugin('bundle.css'),
    //   new webpack.optimize.UglifyJsPlugin(
    //     {minimize: true,compress: {warnings: false}})
    // ],

    plugins: [
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoErrorsPlugin(),
      new ExtractTextPlugin('chatforwork-plugin.css')
    ],

    // อ่านไฟล์นามสกุล .js, .jsx ด้วย Babel
    module: {
        loaders: [
            // {
            //     test: /\.js?$/,
            //     exclude: /node_modules/,
            //     loader: 'babel-loader'
            // },
            
            // {
            //     test: /\.jsx?$/,
            //     exclude: /(node_modules|bower_components)/,
            //     loader: 'babel-loader',
            //     query: {
            //        presets: ['es2015', 'stage-0', 'react'],
            //     }
            // },
            {
                exclude: /node_modules/,
                loader: 'babel',
                test: /\.js$/,
              },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader")
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
            }
        ]
    }
};