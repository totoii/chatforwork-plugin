// if(process.env.NODE_ENV == 'DEV'){
//   module.export = require('./server.dev.js');
// }else{
//   module.export = require('./server.prd.js');
// }


import {graphql} from 'graphql';
import webpack from 'webpack';
import express from 'express';
import cookieParser from  'cookie-parser'
import bodyParser from 'body-parser'
import jwt from 'jsonwebtoken'
import webpackDevMiddleware from 'webpack-dev-middleware'
import graphQLHTTP from 'express-graphql';
import config from './webpack.config'

var app   = express()
var port  = 8080
var port_ssl=1999
var http = require('http');
var https = require('https');
var fs = require('fs');
var options = {
  key: fs.readFileSync('key/server.key'),
  cert: fs.readFileSync('key/server.crt'),
  requestCert: false,
  rejectUnauthorized: false
};
// var httpsServer = https.createServer(options, app);

var http = require('http').Server(app);
// var	io = require('socket.io')(http);
// var io_ssl = require('socket.io')(httpsServer);


if(process.env.NODE_ENV == 'DEV'){
  var compiler = webpack(config)
  app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.publicPath }))
  app.use('/font',express.static(__dirname+'/public/font'))
  // app.use(webpackHotMiddleware(compiler))
}else{
  app.use('/',express.static(__dirname+'/public'))
}

// app.use(cookieParser())
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
// app.use('/display',express.static(__dirname + '/content/image/display'));
// app.use('/content/files',express.static(__dirname + '/content/files'));
// app.use('/',express.static(__dirname + '/node_modules/material-design-lite'));

// app.use('/graphql', graphQLHTTP({schema, pretty: true,graphiql: true}));

// var dbconfig = require('./dbconfig')

// require('./api/auth')(app);
// require('./api/upload')(app);

// app.get('/login', function (req, res){
//   	res.sendFile(__dirname + '/index.html')
// })

// app.get('/logout', function(req, res) {
//   res.clearCookie('at');
//   res.redirect("/");
// });

app.use('/socket.io.js',express.static(__dirname + '/socket.io.js'));

app.get('*', function (req, res){
  res.sendFile(__dirname + '/index.html')
})

// app.get('*', function (req, res){
//   var decoded;
// 	if(req.cookies.at !== null){
//     try {
//         decoded = jwt.verify(req.cookies.at, dbconfig.secret);
//         res.sendFile(__dirname + '/index.html')
//     } catch (e) {
//         decoded= "";
//         res.redirect('/login?path='+req.path);
//     }
// 	}
// })

let io_fn = function(socket){

  console.log("someone connect");
  var m_userId;
  socket.on('file_sended', function(roomid) {
    socket.to(roomid).emit('data_received', {});
  });


  socket.on('disconnect', function(){
    if(m_userId!=null){
      if(socket.adapter.rooms["#user:"+m_userId]==null){
        //User go offline
        console.log(m_userId +" Disconnected",);
      }
    }      
  });
}

// io.on('connection', io_fn);
// io_ssl.on('connection', io_fn);

// httpsServer.listen(port_ssl, function (error) {
//   if (error) {
//     console.error(error)
//   }
// });

http.listen(port, function () {
  console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser. with %s mode", port, port,process.env.NODE_ENV)
});


// http.listen(port, function(error) {
//   if (error) {
//     console.error(error)
//   } else {
//     console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser. with %s mode", port, port,process.env.NODE_ENV)
//   }
// })
